# Krita Twitch - Enable Chat Interactions for Krita

This extension starts up a chatbot in the background that will look for specific commands.

These commands can alter Krita in various ways, such as changing your brush size or what foreground color you are using.

You can view the current list of available commands and what they do [here](#commands).

By default, all chatters except the broadcaster have a 30 second cooldown after using any command. This time can be changed by the artist.

## Setup

Coming soon...

## Commands

> The prefix for commands by default is "?" but this may be changed by the artist.

- __?switchcolors__: Switches the foreground and background color being used by the artist.

- __?fgcolor__ (red) (green) (blue): Changes the foreground being used by the artist to the color that matches the RGB values given.
  - ?fgcolor 255 255 255 "Changes foreground color to white"

- __?bgcolor__ (red) (green) (blue): Same as `?fgcolor` but changes the background color instead.
  - ?bgcolor 0 0 0 "Changes background color to black"

- __?brushsize__ (size): Changes the size of the brush being used by the artist to the given size in pixels.
  - ?brushsize 50 "Changes the size of the brush to 50 pixels"

### Admin Commands

These are commands that can control the bot from chat.

By default, only the broadcaster can use these but it can be configured to allow mods to use them as well.

- __?flushcooldowns__: Resets the cooldown of all chatters for using commands